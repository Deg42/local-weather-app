import { Component } from '@angular/core';
import { WeatherService } from './weather/weather.service';
import { ICurrentWeather } from './interfaces';

@Component({
  selector: 'app-root',
  template: `
    <mat-toolbar color="primary">
      <span>Tiempo local</span>
    </mat-toolbar>
    <div fxLayoutAlign="center">
      <div class="mat-caption vertical-margin">Tu ciudad, tu pronóstico.</div>
    </div>
    <div fxLayoutAlign="center">
      <app-city-search></app-city-search>
    </div>
    <div fxLayout="row">
      <div fxFlex></div>

      <mat-card fxFlex="300px">
        <mat-card-header>
          <mat-card-title>
            <div class="mat-headline">Tiempo actual</div>
          </mat-card-title>
        </mat-card-header>
        <mat-card-content>
          <app-current-weather></app-current-weather>
        </mat-card-content>
      </mat-card>

      <div fxFlex></div>
    </div>
  `,
})
export class AppComponent {
  currentWeather: ICurrentWeather;
  constructor(private weatherService: WeatherService) {}
}
